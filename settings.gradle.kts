// Manual Plugin DSL management
pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
    }
    resolutionStrategy {
        eachPlugin {
            if (requested.id.id.startsWith("com.android")) {
                useModule("com.android.tools.build:gradle:${requested.version}")
            }
            if (requested.id.id == "com.getkeepsafe.dexcount") {
                useModule("com.getkeepsafe.dexcount:dexcount-gradle-plugin:${requested.version}")
            }
        }
    }
}

rootProject.name = "JetpackComposeTest"
rootProject.buildFileName = "build.gradle.kts"

include(":app")
include(":core_ui")
