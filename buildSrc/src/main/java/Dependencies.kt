@file:Suppress("NOTHING_TO_INLINE")

@PublishedApi
internal inline fun plus(base: String, version: String, extra: String?) =
    "${(if (extra == null) base else "$base-$extra")}:$version"

@PublishedApi
internal inline fun set(base: String, version: String, extra: String?) =
    "${(if (extra == null) base else "$base:$extra")}:$version"

object Dependencies {
    const val coreKtx = "androidx.core:core-ktx:${Versions.Libs.coreKtx}"
    const val appCompat = "androidx.appcompat:appcompat:${Versions.Libs.appCompat}"
    const val material = "com.google.android.material:material:${Versions.Libs.material}"
    const val lifeCycle = "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.Libs.lifeCycle}"
    const val dataStorePreferences =
        "androidx.datastore:datastore-preferences:${Versions.Libs.dataStorePreferences}"

    const val composeNavigation =
        "androidx.navigation:navigation-compose:${Versions.JetpackCompose.composeNavigation}"
    const val composeUi = "androidx.compose.ui:ui:${Versions.JetpackCompose.composeVersion}"
    const val composeMaterial =
        "androidx.compose.material:material:${Versions.JetpackCompose.composeVersion}"
    const val composePreview =
        "androidx.compose.ui:ui-tooling-preview:${Versions.JetpackCompose.composeVersion}"
    const val activityCompose =
        "androidx.activity:activity-compose:${Versions.JetpackCompose.composeVersion}"

    object DaggerHilt {
        const val daggerHilt = "com.google.dagger:hilt-android:${Versions.Libs.daggerHilt}"
        const val daggerHiltCompiler =
            "com.google.dagger:hilt-android-compiler:${Versions.Libs.daggerHilt}"
        const val hiltCompiler = "androidx.hilt:hilt-compiler:${Versions.Libs.hiltCompiler}"
    }

    object Room {
        const val room = "androidx.room:room-runtime:${Versions.Libs.room}"
        const val roomCompiler = "androidx.room:room-compiler:${Versions.Libs.room}"
        const val roomKtx = "androidx.room:room-ktx:${Versions.Libs.room}"
    }

}