import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.dsl.DependencyHandler

fun DependencyHandler.applyJetpackCompose(method: (Any) -> Dependency? = ::implementation) {
    method(Dependencies.activityCompose)
    method(Dependencies.composeMaterial)
    method(Dependencies.composeNavigation)
    method(Dependencies.composePreview)
    method(Dependencies.composeUi)
}

fun DependencyHandler.applyLibs(method: (Any) -> Dependency? = ::implementation) {
    method(Dependencies.appCompat)
    method(Dependencies.coreKtx)
    method(Dependencies.lifeCycle)
    method(Dependencies.material)
    method(Dependencies.dataStorePreferences)
}

fun DependencyHandler.applyDaggerHilt(method: (Any) -> Dependency? = ::implementation) {
    method(Dependencies.DaggerHilt.daggerHilt)
    kapt(Dependencies.DaggerHilt.daggerHiltCompiler)
    kapt(Dependencies.DaggerHilt.hiltCompiler)
}

fun DependencyHandler.applyRoom(method: (Any) -> Dependency? = ::implementation) {
    method(Dependencies.Room.room)
    kapt(Dependencies.Room.roomCompiler)
    method(Dependencies.Room.roomKtx)
}