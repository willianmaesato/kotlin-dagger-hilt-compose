object Versions {
    object Libs {
        const val kotlin = "1.5.31"
        const val coreKtx = "1.7.0"
        const val appCompat = "1.3.1"
        const val material = "1.4.0"
        const val lifeCycle = "2.4.0"
        const val dataStorePreferences = "1.0.0"
        const val daggerHilt = "2.36"
        const val hiltCompiler = "1.0.0"
        const val room = "2.2.4"
    }

    object JetpackCompose {
        const val composeNavigation = "2.4.0-alpha06"
        const val composeVersion = "1.0.4"
    }
}