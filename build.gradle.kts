import org.gradle.internal.impldep.org.eclipse.jgit.lib.ObjectChecker.type

// Top-level build file where you can add configuration options common to all sub-projects/modules.
buildscript {
    repositories {
        google()
        jcenter()
        maven{ url = uri("https://jitpack.io")}
    }
    dependencies {
        classpath ("com.android.tools.build:gradle:7.0.3")
        classpath ("org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.Libs.kotlin}")
        classpath ("com.google.dagger:hilt-android-gradle-plugin:${Versions.Libs.daggerHilt}")

        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}


allprojects {
    repositories {
        google()
        mavenCentral()
        maven { url = uri("https://jitpack.io") }
        maven { url = uri ("https://sdk.uxcam.com/android/") }
        flatDir {
            dirs("libs")
        }
    }

    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>().configureEach {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }

    // Necessário até o bug no link abaixo ser corrigido
    // https://github.com/google/dagger/issues/1449
    afterEvaluate {
        // if (project.hasProperty("kapt")) {
        pluginManager.withPlugin("kotlin-kapt") {
            // Reference for "kapt" DSL: https://kotlinlang.org/docs/reference/kapt.html#java-compiler-options
            configure<org.jetbrains.kotlin.gradle.plugin.KaptExtension> {
                javacOptions {
                    option("-source", "11")
                    option("-target", "11")
                }
            }
        }
    }
}

tasks.register("clean", Delete::class){
    delete(rootProject.buildDir)
}