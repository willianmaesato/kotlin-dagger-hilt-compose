package com.example.jetpackcomposetest.id

import android.content.Context
import androidx.room.Room
import com.example.jetpackcomposetest.utils.Constants.DATABASE_NAME
import com.example.jetpackcomposetest.data.DataBase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataBaseModule {

    @Singleton
    @Provides
    fun providerDatabase(
        @ApplicationContext context: Context
    ) = Room.databaseBuilder(
        context, DataBase::class.java, DATABASE_NAME
    ).build()


    @Singleton
    @Provides
    fun provideDao(dataBase: DataBase) = dataBase.toDoDao()

}