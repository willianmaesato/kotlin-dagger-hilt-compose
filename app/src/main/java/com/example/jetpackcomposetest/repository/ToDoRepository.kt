package com.example.jetpackcomposetest.repository

import com.example.jetpackcomposetest.data.dao.ToDoDao
import com.example.jetpackcomposetest.data.models.ToDoTask
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@ViewModelScoped
class ToDoRepository @Inject constructor(private val toDoDao: ToDoDao) {

    val getAllTasks: Flow<List<ToDoTask>> = toDoDao.getAllTasks()

    fun getSelectedTask(taskId: Int): Flow<ToDoTask> {
        return toDoDao.getTaskById(taskId)
    }

    suspend fun addTask(task: ToDoTask) = toDoDao.addTask(task)

    suspend fun updateTask(task: ToDoTask) = toDoDao.updateTask(task)

    suspend fun deleteTask(task: ToDoTask) = toDoDao.deleteTask(task)

    suspend fun deleteAllTasks() = toDoDao.deleteAllTasks()

    fun searchByTitle(searchTask: String): Flow<List<ToDoTask>> {
        return toDoDao.searchTaskByTitle(searchTask)
    }
}