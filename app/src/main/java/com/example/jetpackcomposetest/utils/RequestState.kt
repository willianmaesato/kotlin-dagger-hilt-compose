package com.example.jetpackcomposetest.utils

sealed class RequestState<out T> {
    object NONE : RequestState<Nothing>()
    object LOADING : RequestState<Nothing>()
    data class SUCCESS<T>(val data: T) : RequestState<T>()
    data class ERROR(val error: Throwable) : RequestState<Nothing>()
}
