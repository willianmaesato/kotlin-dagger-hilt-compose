package com.example.jetpackcomposetest.utils

enum class SearchAppBarState {
    OPENED, CLOSED, TRIGGERED
}