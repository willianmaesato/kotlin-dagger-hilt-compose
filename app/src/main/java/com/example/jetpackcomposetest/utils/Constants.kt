package com.example.jetpackcomposetest.utils

object Constants {

    const val DATABASE_TABLE_TODO = "TABLE_TODO"
    const val DATABASE_NAME = "DATABASE_TODO"

    const val LIST_SCREEN = "list/{action}"
    const val TASK_SCREEN = "task/{taskId}"

    const val LIST_ARGUMENT_KEY = "action"
    const val TASK_ARGUMENT_KEY = "taskId"
}