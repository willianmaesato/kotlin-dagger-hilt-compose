package com.example.jetpackcomposetest.utils

enum class Action {
    ADD, UPDATE, DELETE, DELETE_ALL, UNDO, NO_ACTION
}