package com.example.jetpackcomposetest.ui.common

import androidx.compose.foundation.layout.RowScope
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import com.example.core_ui.theme.topAppBarBackgroundColor
import com.example.core_ui.theme.topAppBarContentColor

@Composable
fun CommonAppBar(
    title: String,
    actions: @Composable RowScope.() -> Unit = {},
    navigationIcon: @Composable (() -> Unit)? = null
) {
    TopAppBar(
        navigationIcon = navigationIcon,
        title = {
            Text(
                text = title,
                color = MaterialTheme.colors.topAppBarContentColor
            )
        },
        actions = actions,
        backgroundColor = MaterialTheme.colors.topAppBarBackgroundColor
    )

}