package com.example.jetpackcomposetest.ui.screens.list

import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import com.example.jetpackcomposetest.R
import com.example.core_ui.theme.fabBackgroundColor
import com.example.jetpackcomposetest.ui.viewmodels.SharedViewModel
import com.example.jetpackcomposetest.utils.SearchAppBarState

@ExperimentalMaterialApi
@Composable
fun ListScreen(navigateToTaskScreen: (Int) -> Unit, sharedViewModel: SharedViewModel) {

    LaunchedEffect(key1 = true){
        sharedViewModel.getAllTasks()
    }
    
    val allTasks by sharedViewModel.allTasks.collectAsState()
    val searchAppBarState: SearchAppBarState by sharedViewModel.searchAppBarState
    val searchTextState: String by sharedViewModel.searchTextState

    Scaffold(
        topBar = {
            ListAppBar(
                searchAppBarState = searchAppBarState,
                searchTextState = searchTextState,
                sharedViewModel = sharedViewModel
            )
        },
        content = {
            ListContent(tasks = allTasks, navigateToTaskScreen = navigateToTaskScreen)
        },
        floatingActionButton = {
            ListFab(onFabActionClick = navigateToTaskScreen)
        }
    )
}

@Composable
fun ListFab(
    onFabActionClick: (Int) -> Unit
) {
    FloatingActionButton(
        onClick = {
            onFabActionClick(-1)
        },
        backgroundColor = MaterialTheme.colors.fabBackgroundColor
    ) {
        Icon(
            imageVector = Icons.Filled.Add,
            contentDescription = stringResource(id = R.string.add_button),
            tint = Color.White
        )

    }
}