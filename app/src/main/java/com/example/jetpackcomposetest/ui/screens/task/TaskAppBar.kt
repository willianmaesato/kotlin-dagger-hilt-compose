package com.example.jetpackcomposetest.ui.screens.task

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.example.jetpackcomposetest.R
import com.example.jetpackcomposetest.ui.common.CommonAppBar
import com.example.core_ui.theme.icons.IconAdd
import com.example.core_ui.theme.icons.IconArrowBack
import com.example.jetpackcomposetest.utils.Action

@Composable
fun TaskAppBar(navigateToListScreen: (Action) -> Unit) {
    NewTaskAppBar(navigateToListScreen = navigateToListScreen)
}

@Composable
fun NewTaskAppBar(navigateToListScreen: (Action) -> Unit) {
    CommonAppBar(title = stringResource(id = R.string.add_task), actions = {
        IconAdd(action = { })
    }, navigationIcon = {
        IconArrowBack(action = { navigateToListScreen(Action.NO_ACTION) })

    })
}

@Composable
@Preview
fun NewTaskAppBarPreview() {
    NewTaskAppBar(navigateToListScreen = {})
}