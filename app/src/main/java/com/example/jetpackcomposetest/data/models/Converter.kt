package com.example.jetpackcomposetest.data.models

import androidx.room.TypeConverter

class Converter {
    @TypeConverter
    fun fromPriority(priority: PriorityTask): String {
        return priority.name
    }

    @TypeConverter
    fun toPriority(priority: String): PriorityTask {
        return PriorityTask.valueOf(priority)
    }
}