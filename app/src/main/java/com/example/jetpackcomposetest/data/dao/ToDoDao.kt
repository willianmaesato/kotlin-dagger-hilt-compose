package com.example.jetpackcomposetest.data.dao

import androidx.room.*
import com.example.jetpackcomposetest.data.models.ToDoTask
import kotlinx.coroutines.flow.Flow

@Dao
interface ToDoDao {

    @Query("SELECT * FROM TABLE_TODO ORDER BY id ASC")
    fun getAllTasks(): Flow<List<ToDoTask>>

    @Query("SELECT * FROM TABLE_TODO WHERE id=:taskId")
    fun getTaskById(taskId: Int): Flow<ToDoTask>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addTask(toDoTask: ToDoTask)

    @Update
    suspend fun updateTask(toDoTask: ToDoTask)

    @Delete
    suspend fun deleteTask(toDoTask: ToDoTask)

    @Query("DELETE FROM TABLE_TODO")
    suspend fun deleteAllTasks()

    @Query("SELECT * FROM TABLE_TODO WHERE title OR description LIKE :titleSearch")
    fun searchTaskByTitle(titleSearch: String): Flow<List<ToDoTask>>
}