package com.example.jetpackcomposetest.data.models

import androidx.compose.ui.graphics.Color
import com.example.core_ui.theme.Green600
import com.example.core_ui.theme.Orange600
import com.example.core_ui.theme.Red600
import com.example.core_ui.theme.White600

enum class PriorityTask(val color: Color) {
    HIGH(Red600),
    MEDIUM(Orange600),
    LOW(Green600),
    NONE(White600)
}