package com.example.jetpackcomposetest.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.jetpackcomposetest.data.dao.ToDoDao
import com.example.jetpackcomposetest.data.models.Converter
import com.example.jetpackcomposetest.data.models.ToDoTask

@Database(entities = [ToDoTask::class], version = 1, exportSchema = false)
@TypeConverters(Converter::class)
abstract class DataBase : RoomDatabase() {

    abstract fun toDoDao(): ToDoDao
}