plugins {
    id ("com.android.library")
    id ("kotlin-android")
}

android{
    compileSdk = 31
}

dependencies {
    applyJetpackCompose()
    applyLibs()
}