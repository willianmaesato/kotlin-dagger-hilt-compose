package com.example.core_ui.theme.icons

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import com.example.core_ui.R
import com.example.core_ui.theme.topAppBarContentColor

@Composable
fun IconSearch(action: () -> Unit, modifier: Modifier? = Modifier, tint: Color? = null) {
    IconButton(onClick = { action() }, modifier = modifier ?: Modifier.fillMaxSize()) {
        Icon(
            imageVector = Icons.Filled.Search,
            contentDescription = stringResource(id = R.string.search_icon),
            tint = tint ?: MaterialTheme.colors.topAppBarContentColor
        )
    }
}