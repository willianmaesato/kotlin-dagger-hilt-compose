package com.example.core_ui.theme.icons

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import com.example.core_ui.R
import com.example.core_ui.theme.topAppBarContentColor

@Composable
fun IconAdd(action: () -> Unit, modifier: Modifier? = Modifier, tint: Color? = null) {
    IconButton(onClick = { action() }, modifier = modifier ?: Modifier.fillMaxSize()) {
        Icon(
            imageVector = Icons.Filled.Add,
            contentDescription = stringResource(id = R.string.add_icon),
            tint = tint ?: MaterialTheme.colors.topAppBarContentColor
        )
    }
}