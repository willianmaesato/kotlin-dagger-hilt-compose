package com.example.core_ui.theme

import androidx.compose.material.Colors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

// Bari Primary Colors
val bari_blue = Color(0xFF0090FF)
val bari_black = Color(0xFF101010)
val bari_white = Color(0xFFFFFFFF)

// Neutral Colors
val neutral_gray_0 = Color(0xFFFFFFFF)
val neutral_gray_100 = Color(0xFFF3F3F3)
val neutral_gray_200 = Color(0xFFDBDBDB)
val neutral_gray_300 = Color(0xFFB7B7B7)
val neutral_gray_400 = Color(0xFF878787)
val neutral_gray_500 = Color(0xFF585858)
val neutral_gray_600 = Color(0xFF343434)
val neutral_gray_700 = Color(0xFF1C1C1C)
val neutral_gray_800 = Color(0xFF101010)

// Opacity Grayscale
val opacity_gray_0 = Color(0x00101010)
val opacity_gray_8 = Color(0x14101010)
val opacity_gray_24 = Color(0x3D101010)
val opacity_gray_40 = Color(0x66101010)
val opacity_gray_72 = Color(0xB8101010)
val opacity_gray_88 = Color(0xE0101010)

// Background Colors
val background_neutral_white = Color(0xFFFFFFFF)
val background_neutral_gray_100 = Color(0xFFF3F3F3)
val background_neutral_gray_700 = Color(0xFF1C1C1C)
val background_neutral_gray_800 = Color(0xFF101010)
val background_secondary_blue_500 = Color(0xFF0090FF)

// Text Colors
val text_primary_white = Color(0xFFFFFFFF)
val text_neutral_gray_400 = Color(0xFF878787)
val text_neutral_gray_500 = Color(0xFF585858)
val text_neutral_gray_800 = Color(0xFF101010)
val text_secondary_blue_500 = Color(0xFF0090FF)

val secondary_lilac_600 = Color(0xFF8A2A99)
val secondary_lilac_500 = Color(0xFFB339C5)
val secondary_lilac_400 = Color(0xFFD259E4)
val secondary_lilac_300 = Color(0xFFE074EF)
val secondary_lilac_200 = Color(0xFFF1BDF8)
val secondary_lilac_100 = Color(0xFFF3F0FE)

val secondary_purple_600 = Color(0xFF6139C0)
val secondary_purple_500 = Color(0xFF7343E3)
val secondary_purple_400 = Color(0xFF865AF2)
val secondary_purple_300 = Color(0xFF9972F6)
val secondary_purple_200 = Color(0xFFD1BEFC)
val secondary_purple_100 = Color(0xFFF3F0FE)

val secondary_navy_600 = Color(0xFF2F3E6A)
val secondary_navy_500 = Color(0xFF3E538E)
val secondary_navy_400 = Color(0xFF4764B8)
val secondary_navy_300 = Color(0xFF607DD0)
val secondary_navy_200 = Color(0xFF9DAFE0)
val secondary_navy_100 = Color(0xFFD9DFF2)

val secondary_blue_600 = Color(0xFF0064BE)
val secondary_blue_500 = Color(0xFF0081FF)
val secondary_blue_400 = Color(0xFF0E98FA)
val secondary_blue_300 = Color(0xFF3DA1EF)
val secondary_blue_200 = Color(0xFF8DCDFC)
val secondary_blue_100 = Color(0xFFDDF1FE)

val secondary_moss_600 = Color(0xFF22463B)
val secondary_moss_500 = Color(0xFF2E6B57)
val secondary_moss_400 = Color(0xFF3C8F74)
val secondary_moss_300 = Color(0xFF4FB191)
val secondary_moss_200 = Color(0xFF8ED4BD)
val secondary_moss_100 = Color(0xFFCDECE2)

val secondary_green_600 = Color(0xFF48861B)
val secondary_green_500 = Color(0xFF5DAD26)
val secondary_green_400 = Color(0xFF7ED136)
val secondary_green_300 = Color(0xFF95DE4C)
val secondary_green_200 = Color(0xFFCBF395)
val secondary_green_100 = Color(0xFFEEFBDA)

val secondary_yellow_600 = Color(0xFFD99421)
val secondary_yellow_500 = Color(0xFFFFAC25)
val secondary_yellow_400 = Color(0xFFFFBA2F)
val secondary_yellow_300 = Color(0xFFFFCD43)
val secondary_yellow_200 = Color(0xFFFFE88C)
val secondary_yellow_100 = Color(0xFFFFF7D1)

val secondary_orange_600 = Color(0xFFCE5800)
val secondary_orange_500 = Color(0xFFFF6D02)
val secondary_orange_400 = Color(0xFFFF8332)
val secondary_orange_300 = Color(0xFFFF9B4A)
val secondary_orange_200 = Color(0xFFFFCD9A)
val secondary_orange_100 = Color(0xFFFFF0DE)

val secondary_red_600 = Color(0xFFC72127)
val secondary_red_500 = Color(0xFFF63238)
val secondary_red_400 = Color(0xFFFF5B60)
val secondary_red_300 = Color(0xFFFF787A)
val secondary_red_200 = Color(0xFFFFBBBD)
val secondary_red_100 = Color(0xFFFFF1F1)

val secondary_rose_600 = Color(0xFF9F1641)
val secondary_rose_500 = Color(0xFFD6275E)
val secondary_rose_400 = Color(0xFFF55586)
val secondary_rose_300 = Color(0xFFFD739F)
val secondary_rose_200 = Color(0xFFFFB3CC)
val secondary_rose_100 = Color(0xFFFFEAF3)

val lightGray = Color(0xFFFCFCFC)
val MediumGray = Color(0xFF9C9C9C)
val DarkGray = Color(0xFF141414)

val Red600 = Color(0xFFF63238)
val Orange600 = Color(0xFFF39C12)
val Green600 = Color(0xFF2ECC71)
val White600 = Color(0xFFECF0F1)

val Colors.taskItemTextColor: Color
    @Composable
    get() = if (isLight) DarkGray else lightGray

val Colors.taskItemBackgroundColor: Color
    @Composable
    get() = if (isLight) Color.White else DarkGray

val Colors.fabBackgroundColor: Color
    @Composable
    get() = if (isLight) Teal200 else Purple700

val Colors.topAppBarContentColor: Color
    @Composable
    get() = if (isLight) Color.White else lightGray

val Colors.topAppBarBackgroundColor: Color
    @Composable
    get() = if (isLight) Purple500 else Color.Black